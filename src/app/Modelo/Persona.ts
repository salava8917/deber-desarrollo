export class Persona{
    idpersona:number;
    cedula:String;
    nombre:String;
    fecha_nacimiento:Date;
    direccion:String;
    telefono:String;
}